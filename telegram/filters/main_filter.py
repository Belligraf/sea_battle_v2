from aiogram.types import Message, CallbackQuery
from main import bot
from telegram.data.text_for_handlers import dictionary, new_player, game_started
from errors.errors import MyError
from data_base import functions_for_work_with_bd
from logic import logic_for_aiogram, phases
from data_base.db import cursor
from telegram.handlers import commands


async def start_filter(message: Message):
    t_id = message.from_user.id

    # if player in the next phase
    cursor.execute(f"SELECT player_phase FROM phase WHERE t_id = {t_id}")
    phase = cursor.fetchone()
    if phase:
        if phase[0] != 'in_queue':
            return

    # if player already in queue
    try:
        result = functions_for_work_with_bd.insert_player_to_queue(message)  # [(666),(555)] or False
    except MyError as err:
        await bot.send_message(t_id, *err.args)
        return

    # if only one player in the queue
    if not result:
        await bot.send_message(t_id, dictionary['/start'])

        nickname = message.from_user.username
        text_for_admin = new_player(nickname)
        await commands.send_to_admin(dispatcher=None, text_for_admin=text_for_admin)

    # start game
    else:
        cursor.execute("SELECT nickname from in_queue")
        nicknames = cursor.fetchall()
        nickname_1 = nicknames[0][0]
        nickname_2 = nicknames[1][0]
        text_for_admin = game_started(nickname_1=nickname_1, nickname_2=nickname_2)
        print(text_for_admin)

        await commands.send_to_admin(dispatcher=None, text_for_admin=text_for_admin)
        await logic_for_aiogram.start_game(t_id=t_id)


async def filter_callback(callback: CallbackQuery):
    await callback.answer(cache_time=1)
    cursor.execute(f"SELECT player_phase FROM phase WHERE t_id = {callback.from_user.id}")
    phase = cursor.fetchone()[0]
    print(phase)

    if phase == phases.phase_2:
        await logic_for_aiogram.call_back_for_phase_2(callback=callback)

    elif phase == phases.phase_4 and callback.data == 'surrender':
        await logic_for_aiogram.end_game(callback, surrender=True)

    elif phase == phases.phase_3 and callback.data == 'cancel':
        await logic_for_aiogram.cancel_game(callback)

    elif phase == phases.phase_4:
        await logic_for_aiogram.call_back_for_phase_4(callback)
