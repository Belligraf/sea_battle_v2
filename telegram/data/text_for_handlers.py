dictionary = {'/start': 'Идёт поиск игры, ожидайте...',
              'game_found': 'Противник найден игра началась',
              '/help': 'Такие-то правила'}

end_line_button_random = 'Рандомная расстановка'
end_line_button_ready = 'Готов'
end_line_button_surrender = 'Сдаться'
end_line_button_cancel = 'Отмена игры'

not_admin = 'У вас нет доступа'
cancel = 'Игра отменена'


def winner_text(nickname):
    text = f'Поздравляем, вы унизили {nickname}'
    return text


def looser_text(nickname):
    text = f'Сожалеем, вы были унижены by {nickname}'
    return text


def your_enemy(nickname):
    text = f'Ваш противник: @{nickname}'
    return text


def new_player(nickname):
    text = f'@{nickname} начал поиск игры'
    return text


def game_started(nickname_1, nickname_2):
    text = f'@{nickname_1} и @{nickname_2} начали игру'
    return text
