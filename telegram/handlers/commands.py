from main import dp, bot

from classes import admin_class
from telegram.data import config
from aiogram.types import Message
from telegram.data.config import admin_id
from telegram.data import text_for_handlers
from telegram import filters


@dp.message_handler(commands=['get_queue', 'get_players', 'clean_queue', 'clean_table', 'admin'])
async def commands_for_admin(message: Message):
    t_id = message.from_user.id

    if not t_id == config.admin_id:
        await bot.send_message(chat_id=t_id,
                               text=text_for_handlers.not_admin)
        return
    text_for_admin = admin_class.dict_for_filter[message.text]()

    await bot.send_message(chat_id=t_id,
                           text=text_for_admin)


async def send_to_admin(dispatcher, text_for_admin=None):
    if not text_for_admin:
        text_for_admin = "Бот запущен"

    await bot.send_message(chat_id=admin_id, text=text_for_admin)


@dp.message_handler(commands=['start'])
async def start(message: Message):
    await filters.start_filter(message)
