from main import dp, bot
from aiogram.types import CallbackQuery
from telegram.keyboards.inline import keyboard_generator
from telegram.filters import main_filter
from classes import some_classes
from data_base.db import cursor, conn
from aiogram.types import InlineKeyboardButton
from telegram.data.text_for_handlers import end_line_button_surrender, cancel, your_enemy


# Send keyboard with field to user
async def phase_place_ships(player: some_classes.Player,
                            enemy: some_classes.Player):
    keyboard = keyboard_generator.get_keyboard_phase_2(player)
    text_for_user = your_enemy(enemy.nickname)
    msg_with_keyboard = await bot.send_message(chat_id=player.t_id,
                                               text=text_for_user,
                                               reply_markup=keyboard)

    cursor.execute("INSERT INTO messages VALUES(?, ?)", (msg_with_keyboard.message_id,
                                                         player.t_id))
    conn.commit()

    '''await bot.edit_message_reply_markup(chat_id=message.from_user.id,
                                        message_id=msg_with_keyboard.message_id,
                                        reply_markup=keyboard_2)'''


@dp.callback_query_handler()
async def get_callback(call: CallbackQuery):
    await call.answer(cache_time=1)

    await main_filter.filter_callback(callback=call)

    # callback_data = call.data

    # await bot.send_message(call.from_user.id, callback_data)


async def edit_message_after_ready(player):
    cursor.execute(f"SELECT * FROM messages WHERE t_id = {player.t_id}")
    msg_id = cursor.fetchone()[0]
    keyboard = keyboard_generator.get_keyboard_phase_3(player)
    await bot.edit_message_reply_markup(chat_id=player.t_id,
                                        message_id=msg_id,
                                        reply_markup=keyboard)


async def edit_message_after_someone_win(player, enemy_id, function_for_text):
    cursor.execute(f"SELECT * FROM messages WHERE t_id = {player.t_id}")
    msg_id = cursor.fetchone()[0]

    cursor.execute(f"SELECT nickname FROM player WHERE t_id = {enemy_id}")
    enemy_nickname = cursor.fetchone()[0]
    text = function_for_text(enemy_nickname)

    await bot.edit_message_text(chat_id=player.t_id,
                                message_id=msg_id,
                                text=text)


async def edit_message_after_hit(player, player_for_hit):
    cursor.execute(f"SELECT * FROM messages WHERE t_id = {player.t_id}")
    button_surrender = InlineKeyboardButton(text=end_line_button_surrender, callback_data=f'surrender')
    msg_id = cursor.fetchone()[0]
    if player.t_id != player_for_hit.t_id:
        keyboard = keyboard_generator.get_actual_keyboard(player_for_hit, invisible=True)
    else:
        keyboard = keyboard_generator.get_actual_keyboard(player_for_hit)

    keyboard.insert(button_surrender)
    await bot.edit_message_reply_markup(chat_id=player.t_id,
                                        message_id=msg_id,
                                        reply_markup=keyboard)


async def edit_message_after_random(player):
    cursor.execute(f"SELECT * FROM messages WHERE t_id = {player.t_id}")
    msg_id = cursor.fetchone()[0]
    keyboard = keyboard_generator.get_keyboard_phase_2(player)
    await bot.edit_message_reply_markup(chat_id=player.t_id,
                                        message_id=msg_id,
                                        reply_markup=keyboard)


async def edit_message_after_someone_cancel(player):
    cursor.execute(f"SELECT * FROM messages WHERE t_id = {player.t_id}")
    msg_id = cursor.fetchone()[0]

    await bot.edit_message_text(chat_id=player.t_id,
                                message_id=msg_id,
                                text=cancel)
