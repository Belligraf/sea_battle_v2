from aiogram import Bot, Dispatcher, executor
import asyncio
from telegram.data import config


loop = asyncio.get_event_loop()
bot = Bot(config.token)
dp = Dispatcher(bot, loop=loop)


def main():
    pass


if __name__ == '__main__':
    from telegram.handlers import dp, send_to_admin
    executor.start_polling(dp, on_startup=send_to_admin)
    # main()
