from data_base.db import cursor, conn


def get_queue() -> str:
    cursor.execute("SELECT * FROM in_queue")
    result = cursor.fetchall()
    result = ' \n'.join(nick[1] for nick in result)
    return result


def get_players():
    cursor.execute("SELECT * FROM player")
    result = cursor.fetchall()
    result = ' \n'.join(['@' + nick[1] for nick in result])
    return result


def clean_queue():
    cursor.execute("DELETE FROM in_queue;")
    conn.commit()
    return 'Successfully'


def clean_table():
    cursor.execute("DELETE FROM in_queue;")
    cursor.execute("DELETE FROM field;")
    cursor.execute("DELETE FROM games;")
    cursor.execute("DELETE FROM phase;")
    cursor.execute("DELETE FROM player;")
    cursor.execute("DELETE FROM sea_battle_game;")
    cursor.execute("DELETE FROM messages;")
    cursor.execute("DELETE FROM ships;")
    conn.commit()
    return 'Successfully'


def admin():
    return '\n\n'.join(['/get_queue', '/get_players', '/clean_queue', '/clean_table', '/admin'])


dict_for_filter = {'/get_queue': get_queue,
                   '/get_players': get_players,
                   '/clean_queue': clean_queue,
                   '/clean_table': clean_table,
                   '/admin': admin}
